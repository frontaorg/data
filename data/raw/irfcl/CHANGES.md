Dataset downloaded from: <https://data.imf.org/?sk=2DFB3380-3603-4D2C-90BE-A04D8BBCE237&sId=1452811319782>

## Terms of use

SPECIAL TERMS AND CONDITIONS PERTAINING TO THE USE OF DATA

Notwithstanding the general prohibition on the commercial use of IMF content, with respect to published statistical data made available on IMF Sites, the following special terms shall govern.

For the purposes of these special terms, the term “Data” refers to any published statistical data produced by the IMF that is not owned by a third-party. In particular, “Data” refers to the following:

- IMF Statistical Data, including but not limited to, International Financial Statistics (IFS), Balance of Payments (BOP), Direction of Trade (DOT), and Government Finance Statistics (GFS) ( http://data.imf.org/);
- World Economic Outlook database ;
- Primary Commodity Prices ;
- IMF Financial Data ;
- Exchange Rate Data ; and
- And any other statistical data available on www.imf.org that explicitly identify the International Monetary Fund as the source.

Users may download, extract, copy, create derivative works, publish, distribute, and sell Data obtained from IMF Sites, including for commercial purposes, subject to the following conditions:

- When Data is distributed or reproduced, it must appear accurately and attributed to the IMF as the source, e.g. “Source: International Monetary Fund.” This source attribution requirement is attached to any use of IMF Data, whether obtained directly from the IMF or from a User.
- Users who make IMF Data available to other users through any type of distribution or download environment agree to take reasonable efforts to communicate and promote compliance by their end users with these terms.
- The Data is provided to Users “as is” and without warranty of any kind, either express or implied, including, without limitation, warranties of merchantability, fitness for a particular purpose, and noninfringement.
- If IMF Data is sold by Users as a standalone product; sellers must inform purchasers that the Data is available free of charge from the IMF.
- Users shall not infringe upon the integrity of the Data and in particular shall refrain from any act of alteration of the Data that intentionally affects its nature or accuracy. If the Data is materially transformed by the User, this must be stated explicitly along with the required source citation.
- The policy of free access and free reuse does not imply a right to obtain confidential or any unpublished underlying data, over which the IMF reserves all rights.

Except as stated in this Section VIII, all other terms set forth in the general terms and conditions shall continue to apply to use of IMF Data.
