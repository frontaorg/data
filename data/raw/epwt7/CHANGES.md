# Original file changes

Dataset downloaded from <https://dataverse.harvard.edu/file.xhtml?fileId=6022652&version=1.1>

## Terms of use

Datasheet itself has **attribution requirement** with following reference:

_Marquetti, A., Morrone, H., and Miebach, A. (2021). The Extended Penn World Tables 7.0. Texto para Discussão 2021/01, UFRGS._

Downloaded from wider dataset with following references:

_Basu, Deepankar, 2022, "World Profit Rates, 1960-2019", https://doi.org/10.7910/DVN/2OL4IW, Harvard Dataverse, V1; EPWT 7.0 FV.xlsx [EPWT 7.0 FV.xlsx]_

_Basu, Deepankar, 2022, "World Profit Rates, 1960-2019", https://doi.org/10.7910/DVN/2OL4IW, Harvard Dataverse, V1_
