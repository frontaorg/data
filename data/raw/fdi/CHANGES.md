# Original file changes

Dataset downloaded from <https://stats.oecd.org/index.aspx#>

From table:

- Globalisation
  - FDI statistics according to Benchmark Definition 4th Edition (BMD4)
    - FDI statistics by partner country and by industry - Summary
      - FDI statistics by partner country and by industry - Summary

Filter selected: For years 2005-2021 (max available at time).

## Terms of use

<https://www.oecd.org/termsandconditions/>

### Data source(s) used

Source: OECD International direct investment database

FDI data are based on statistics provided by 38 OECD member countries.

The last major update of the database was performed on 20 December 2022.

### Permitted use

Except where additional restrictions apply as stated above, You can extract from, download, copy, adapt, print, distribute, share and embed Data for any purpose, even for commercial use. You must give appropriate credit to the OECD by using the citation associated with the relevant Data, or, if no specific citation is available, You must cite the source information using the following format: OECD (year), (dataset name),(data source) DOI or URL (accessed on (date)). When sharing or licensing work created using the Data, You agree to include the same acknowledgment requirement in any sub-licenses that You grant, along with the requirement that any further sub-licensees do the same.
