# Original file changes

Repository link: <https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes>

Dataset downloaded from <https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv>

Attribution: [Luke Duncalfe](https://github.com/lukes)
