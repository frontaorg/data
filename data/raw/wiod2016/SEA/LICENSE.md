Creative Commons Attribution 4.0 International License:
https://creativecommons.org/licenses/by/4.0/

## Reference requirement:

Timmer, M. P., Dietzenbacher, E., Los, B., Stehrer, R. and de Vries, G. J. (2015),
["An Illustrated User Guide to the World Input–Output Database: the Case of Global Automotive Production"](http://dx.doi.org/10.1111/roie.12178) ,
Review of International Economics., 23: 575–605
