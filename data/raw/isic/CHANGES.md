Datasets downloaded from: <https://unstats.un.org/unsd/classifications/Econ/isic>

## Direct links:

- [./ISIC_Rev_3_1_english_structure.txt](https://unstats.un.org/unsd/classifications/Econ/Download/In%20Text/ISIC_Rev_3_1_english_structure.txt)
- [./ISIC_Rev_3_english_structure.txt](https://unstats.un.org/unsd/classifications/Econ/Download/In%20Text/ISIC_Rev_3_english_structure.txt)
- [./ISIC_Rev_3-ISIC_Rev_3_1_correspondence.txt](https://unstats.un.org/unsd/classifications/Econ/tables/ISIC/ISIC3_ISIC31/ISIC_Rev_3-ISIC_Rev_3_1_correspondence.txt)
- [./ISIC_Rev_4_english_structure.Txt](https://unstats.un.org/unsd/classifications/Econ/Download/In%20Text/ISIC_Rev_4_english_structure.Txt)
- [./ISIC31_ISIC4.txt](https://unstats.un.org/unsd/classifications/Econ/tables/ISIC/ISIC31_ISIC4/ISIC31_ISIC4.txt)

## Changes:

- Renamed ./ISIC_Rev_3_1_english_structure.txt to ./ISIC_Rev_3_1_english_structure.csv
- Renamed ./ISIC_Rev_3-ISIC_Rev_3_1_correspondence.txt to ./ISIC_Rev_3-ISIC_Rev_3_1_correspondence.csv
- Renamed ./ISIC_Rev_4_english_structure.Txt to ./ISIC_Rev_4_english_structure.csv
- Renamed ./ISIC31_ISIC4.txt to ./ISIC31_ISIC4.csv
- Converted ./ISIC_Rev_3_english_structure.txt from fixed-width columns to tsv with Excel, renamed to ./ISIC_Rev_3_english_structure.tsv
