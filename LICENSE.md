# License

Various folders in this repository are licensed differently.
`LICENSE.txt` in each folder specifies license for this folder.

Summary:

- `data/raw`: Each data source has its own licensing requirements. Check subfolders.
- `data/out`: TODO
- `book`: GNU Free Documentation License Version 1.3
- `src`: GNU AFFERO GENERAL PUBLIC LICENSE Version 3
