# Fronta.org data

Fronta.org datasets and accompanying documentation.

---

<!-- To update: `npx doctoc ./README.md` -->
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Building locally](#building-locally)
  - [Documentation](#documentation)
  - [Data](#data)
- [Raw datasets](#raw-datasets)
- [Forked from](#forked-from)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Building locally

You will need `git` and `Python:3.10`.

```
git clone https://gitlab.com/frontaorg/data.git
cd data
git lfs fetch origin main
```

### Documentation

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

jupyter-book build book
```

To open book now navigate to `book/_build/html` and double-click `index.html`.

Read more at Jupyter Book's [Introduction](https://jupyterbook.org/intro.html).

### Data

Datasets are in `data/out` folder. Raw data files are in `data/raw` folder.

## Raw datasets

Are reproduced in `data/raw` folder with appropriate license notices. Our
changes and data source links are in `*/CHANGES.md` files.

## Forked from

Forked from <https://gitlab.com/pages/jupyterbook>, which was
Forked from @NikosAlexandris -- Thank you @stingrayza
