# Datasets

```{caution} Datasets are still preliminary. Verify data before serious usage.

```

## File format

Datasets are provided in [Parquet](https://parquet.apache.org/) format with `gzip` compression.

## Importing datasets

### Pandas

In addition to `pandas` you need to install `fastparquet` or `pyarrow`.

```python
import pandas as pd

df = pd.read_parquet("example.parquet.gzip")
```

### Polars

```python
import polars as pl

df = pl.read_parquet("example.parquet.gzip")

# Or for lazy loading:
df = pl.scan_parquet("example.parquet.gzip")
```

### R

```{caution} Not tested.

```

You will need to install `arrow` package.

```R
library(arrow)

read_parquet("example.parquet.gzip", as_tibble = TRUE)
```

See this [question](https://stackoverflow.com/questions/30402253/how-do-i-read-a-parquet-in-r-and-convert-it-to-an-r-dataframe) on stackoverflow.

### DuckDB

```{caution} Not tested.

```

```SQL
SELECT * FROM 'example.parquet.gzip';
```

See [documentation](https://duckdb.org/docs/data/parquet.html).

## Table of Contents

```{tableofcontents}

```
