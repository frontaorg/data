import xml.etree.ElementTree as ET
from pathlib import Path
from shutil import rmtree
from zipfile import ZipFile

import pandas as pd
import typer

from common.cc import read_cc
from common.type import asstring

raw_path = Path("data/raw/unicode_core/common/main.zip")

temp_path = Path("workdir/unicode_core")

out_path = Path("data/out/country_translation_table.parquet.gzip")


def check():
    """Check if raw and workdir exists and/or are okay."""
    if not (raw_path.exists() and raw_path.is_file()):
        raise Exception("Raw file doesn't exist or isn't file!")

    if temp_path.exists() and not temp_path.is_dir():
        raise Exception("Temp path is not directory!")

    if out_path.exists() and not out_path.is_file():
        raise Exception("Outfile exists and is not file!")


def prepare():
    """Prepare workdir."""
    if not temp_path.exists():
        temp_path.mkdir()

    for child in temp_path.iterdir():
        # Clean temp directory.
        if child.is_dir():
            rmtree(child)
        else:
            child.unlink()


def check_xml(path: Path):
    """Check if xml file is okay."""
    if not path.is_file:
        raise Exception(f"Unexpected not-file in workdir: {path}!")

    if not path.suffix.lower() == ".xml":
        raise Exception(f"Unexpected not-xml file in workdir: {path}!")


def parse_xml(path: Path):
    """Extract from xml file to list of pandas series translation data."""
    tree = ET.parse(path)
    root = tree.getroot()

    territories_list = list(root.iterfind("localeDisplayNames/territories"))

    # We skip files without these tags. "Country" files have them.

    if len(territories_list) != 1:
        return

    territories = territories_list[0]

    territory_parsed = [
        pd.Series(
            data=[territory.get(key="type"), territory.text, path.name.split(".")[0]],
            index=["territory", "name", "language"],
        )
        for territory in territories.iterfind("territory")
        if territory.get(key="alt") == None
    ]

    return territory_parsed


def main():
    # We are going to grab localized territory names for each language.
    # So we can construct translation table for countries.

    check()
    prepare()

    # First we unzip.
    with ZipFile(raw_path, "r") as zip:
        zip.extractall(temp_path)

    series = []

    for filepath in temp_path.iterdir():
        check_xml(filepath)
        maybe_series = parse_xml(filepath)

        if maybe_series != None:
            series = [*series, *maybe_series]

    # Translation table with raw lang and territory codes.
    tt_raw = pd.DataFrame(series)

    # Language codes we are going to keep as they are.

    # Need to onvert territory codes.
    # Country codes are in ISO 3166-1 alpha-2 standard.
    # Our datasets are normalized in ISO 3166-1 numeric standard.

    # First we drop non-country rows. While countries are in alpha-2
    # those are three-digit strings.

    tt_filtered = tt_raw[tt_raw["territory"].str.len() == 2]

    # Get conversion table
    cc = read_cc()

    tt_with_cc = pd.merge(
        tt_filtered, cc, how="left", left_on="territory", right_on="alpha-2"
    )

    tt = tt_with_cc[["countrycode", "language", "name"]].dropna()

    tt["language"] = asstring(tt["language"])
    tt["name"] = asstring(tt["name"])

    # Delete output file if exists.
    if out_path.exists():
        out_path.unlink()

    # Finally we save to parquet.
    tt.to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
