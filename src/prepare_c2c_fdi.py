from pathlib import Path

import pandas as pd
import typer

from common.cc import read_cc
from common.type import asint, asreal

fdi_path = Path("data/raw/fdi/FDI_CTRY_IND_SUMM_07022023134357387.csv")

out_path = Path("data/out/c2c_fdi.parquet.gzip")


def main():
    df = pd.read_csv(fdi_path)

    df["alpha-3"] = df["COU"]
    df["counterpart_alpha-3"] = df["COUNTERPART_AREA"]
    df = df[df["MEASURE"] == "USD"]

    df["value"] = df["Value"] * 1_000_000

    out_columns = ["alpha-3", "counterpart_alpha-3", "Year", "FDI_TYPE", "value"]

    df["is_inward"] = df["Measurement principle"] == "Directional principle: Inward"

    df.loc[df["is_inward"], "FDI_TYPE"] += "_in"
    df.loc[~df["is_inward"], "FDI_TYPE"] += "_out"

    thin = df[out_columns].drop_duplicates().dropna(subset=["value"])

    grouped = (
        thin.groupby(["alpha-3", "counterpart_alpha-3", "Year", "FDI_TYPE"])["value"]
        .aggregate("mean")
        .unstack()
        .reset_index()
    )

    cc = read_cc()

    val_columns = [
        col
        for col in grouped.columns
        if col not in ["alpha-3", "counterpart_alpha-3", "Year"]
    ]

    fdi_with_c1 = pd.merge(
        left=grouped, right=cc, how="left", left_on=["alpha-3"], right_on=["alpha-3"]
    )
    fdi_with_cc = pd.merge(
        left=fdi_with_c1,
        right=cc,
        how="left",
        left_on=["counterpart_alpha-3"],
        right_on=["alpha-3"],
        suffixes=("", "_counterpart"),
    ).dropna(subset=["countrycode", "countrycode_counterpart"])

    fdi_with_cc["countrycode"] = asint(fdi_with_cc["countrycode"])
    fdi_with_cc["counterpart_countrycode"] = asint(
        fdi_with_cc["countrycode_counterpart"]
    )
    fdi_with_cc["year"] = asint(fdi_with_cc["Year"])

    for col in val_columns:
        fdi_with_cc[col] = asreal(fdi_with_cc[col])

    out_columns = ["countrycode", "counterpart_countrycode", "year", *val_columns]

    result = fdi_with_cc[out_columns].reset_index()

    result.to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
