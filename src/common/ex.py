from pathlib import Path

import pandas as pd

from common.cc import read_cc
from common.type import asint, asreal

ex_path = Path("data/raw/oecd-ex/DP_LIVE_03022023130841398.csv")
ppp_path = Path("data/raw/oecd-ppp/DP_LIVE_03022023130857804.csv")


def read_ex():
    cc = read_cc()

    raw_columns = ["LOCATION", "TIME", "Value"]
    key_columns = ["LOCATION", "TIME"]

    ex = pd.read_csv(ex_path)[raw_columns]
    ppp = pd.read_csv(ppp_path)[raw_columns]

    conv = pd.merge(
        ex,
        ppp,
        how="outer",
        left_on=key_columns,
        right_on=key_columns,
        suffixes=("_ex", "_ppp"),
    )

    conv_with_cc = pd.merge(
        cc, conv, how="left", left_on=["alpha-3"], right_on=["LOCATION"]
    ).dropna(subset=["TIME"])

    conv_with_cc["year"] = asint(conv_with_cc["TIME"])

    conv_with_cc["ex"] = asreal(conv_with_cc["Value_ex"])
    conv_with_cc["ppp"] = asreal(conv_with_cc["Value_ppp"])

    out_columns = ["countrycode", "year", "ex", "ppp"]

    result = conv_with_cc[out_columns]

    return result
