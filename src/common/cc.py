from pathlib import Path

import pandas as pd

from common.type import asstring

cc_filepath = Path("data/raw/iso-3166/all.csv")


def read_cc():
    df = pd.read_csv(cc_filepath)

    df["countrycode"] = df["country-code"]
    df["alpha-2"] = asstring(df["alpha-2"])
    df["alpha-2"] = asstring(df["alpha-3"])

    return df[["alpha-2", "alpha-3", "countrycode"]]
