import pandas as pd


def asint(col: pd.Series):
    return pd.to_numeric(col, downcast="integer", errors="coerce").astype(int)


def asint_ig(col: pd.Series):
    return pd.to_numeric(col, downcast="integer", errors="ignore").fillna(0).astype(int)


def asreal(col: pd.Series):
    return pd.to_numeric(col, downcast="float", errors="coerce").astype(float)


def asstring(col: pd.Series):
    return col.astype("|S")
