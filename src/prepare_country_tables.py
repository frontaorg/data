from pathlib import Path

import pandas as pd
import typer

from common.cc import read_cc
from common.ex import read_ex
from common.type import asint, asreal

epwt7_path = Path("data/raw/epwt7/EPWT 7.0 FV.xlsx")

out_path = Path("data/out/country_tables.parquet.gzip")


def clean_epwt7():
    cc = read_cc()
    ex = read_ex()

    epwt7_raw = pd.read_excel(epwt7_path, "EPWT7.0")

    epwt7_cc = pd.merge(
        left=epwt7_raw,
        right=cc[["alpha-3", "countrycode"]],
        how="left",
        left_on=["Countrycode"],
        right_on=["alpha-3"],
    )

    epwt7_ex = pd.merge(
        left=epwt7_cc,
        right=ex,
        how="left",
        left_on=["countrycode", "Year"],
        right_on=["countrycode", "year"],
    )

    int_cols = ["Year"]
    real_cols = [
        "Population",
        "N",
        "LabShare",
        "E",
        "B",
        "delta",
        "XGDPnatcur",
        "Knatcur",
        "xnatcur",
        "rhonatcur",
        "knatcur",
        "inatcur",
        "cnatcur",
        "rnatcur",
        "wnatcur",
    ]

    for col in int_cols:
        epwt7_ex[col] = asint(epwt7_ex[col])

    for col in real_cols:
        epwt7_ex[col] = asreal(epwt7_ex[col])

    renames = [
        {"from": "Year", "to": "year"},
    ]

    for col in renames:
        epwt7_ex[col["to"]] = epwt7_ex[col["from"]]

    convert_cols = [
        "XGDPnatcur",
        "Knatcur",
        "xnatcur",
        "rhonatcur",
        "knatcur",
        "inatcur",
        "cnatcur",
        "rnatcur",
        "wnatcur",
    ]

    for col in convert_cols:
        epwt7_ex[col.replace("natcur", "")] = epwt7_ex[col] / epwt7_ex["ex"]

    keep_cols = [
        "countrycode",
        "year",
        "Population",
        "N",
        "LabShare",
        "E",
        "B",
        "delta",
        "XGDP",
        "K",
        "x",
        "rho",
        "k",
        "i",
        "c",
        "r",
        "w",
    ]

    epwt7 = epwt7_ex[keep_cols].copy()

    epwt7.dropna(
        subset=[
            "Population",
            "N",
            "LabShare",
            "E",
            "B",
            "delta",
            "XGDP",
            "K",
            "x",
            "rho",
            "k",
            "i",
            "c",
            "r",
            "w",
        ],
        how="all",
        inplace=True,
    )

    return epwt7


def main():
    epwt7 = clean_epwt7()

    epwt7.to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
