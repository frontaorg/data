from pathlib import Path

import typer

from common.ex import read_ex

out_path = Path("data/out/currency_conversion.parquet.gzip")


def main():
    ex = read_ex()

    ex.to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
