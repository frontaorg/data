from pathlib import Path

import typer

from common.cc import read_cc

out_path = Path("data/out/iso-3166.parquet.gzip")


def main():
    cc = read_cc()

    cc.to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
