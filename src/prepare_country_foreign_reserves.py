from pathlib import Path

import pandas as pd
import typer

from common.cc import read_cc
from common.type import asint, asint_ig, asreal, asstring

irfcl_path = Path("data/raw/irfcl/IRFCL_02-07-2023 09-33-10-07_timeSeries.csv")
irfcl_meta_path = Path(
    "data/raw/irfcl/Metadata_IRFCL_02-07-2023 11-07-14-13_timeSeries.csv"
)

out_path = Path("data/out/country_foreign_reserves.parquet.gzip")


def create_imf_cc(metapath: Path):
    metadata = pd.read_csv(metapath)
    alpha_3 = metadata[metadata["Metadata Attribute"] == "Country ISO 3 Code"][
        ["Country Code", "Metadata Value"]
    ]
    cc = read_cc()

    cc_conv = pd.merge(
        left=alpha_3,
        right=cc,
        how="left",
        left_on=["Metadata Value"],
        right_on=["alpha-3"],
    )
    cc_conv["IMFcode"] = cc_conv["Country Code"]

    return cc_conv[["IMFcode", "countrycode"]]


def clean_irfcl(path: Path, metapath: Path):
    id_vars = [
        "Country Name",
        "Country Code",
        "Indicator Name",
        "Indicator Code",
        "Sector Name",
        "Sector Code",
    ]
    value_vars = [f"{year}" for year in range(1995, 2023)]
    cols = id_vars + value_vars

    df = pd.read_csv(path)[cols]

    # First we need to melt year columns.
    melted = pd.melt(
        df, id_vars=id_vars, value_vars=value_vars, var_name="year", ignore_index=True
    ).dropna(subset=["value"])

    thin = melted[["Country Code", "Indicator Code", "Sector Code", "year", "value"]]

    # And then pivot by Indicator Code.
    pivoted = (
        thin.groupby(["Country Code", "Sector Code", "year", "Indicator Code"])["value"]
        .aggregate("mean")
        .unstack()
        .reset_index()
    )

    value_cols = [
        x
        for x in pivoted.columns
        if not x in ["Indicator Code", "Country Code", "Sector Code", "year"]
    ]

    pivoted = pivoted.dropna(subset=value_cols, how="all")

    pivoted["sectorcode"] = asstring(pivoted["Sector Code"])

    cc = create_imf_cc(metapath)

    pivoted_cc = pd.merge(
        left=pivoted,
        right=cc,
        how="left",
        left_on=["Country Code"],
        right_on=["IMFcode"],
    )

    out_columns = [
        col
        for col in pivoted_cc.columns
        if col not in ["Sector Code", "Country Code", "IMFcode"]
    ]

    result = pivoted_cc[out_columns].dropna(subset=["countrycode", "year"])
    result["countrycode"] = asint_ig(result["countrycode"])
    result["year"] = asint(result["year"])

    other_cols = [
        col for col in out_columns if col not in ["countrycode", "sectorcode", "year"]
    ]

    for col in other_cols:
        result[col] = asreal(result[col])

    # Reorder columns.
    result = result[["countrycode", "sectorcode", "year", *other_cols]]

    return result


def main():
    irfcl = clean_irfcl(irfcl_path, irfcl_meta_path)

    irfcl.to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
