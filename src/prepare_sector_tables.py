from pathlib import Path

import pandas as pd
import typer

from common.cc import read_cc
from common.ex import read_ex
from common.type import asint, asreal

sea_path = Path("data/raw/wiod2016/SEA/Socio_Economic_Accounts.xlsx")
out_path = Path("data/out/sector_tables.parquet.gzip")


def main():
    sea_raw = pd.read_excel(sea_path, sheet_name="DATA")
    cc = read_cc()
    ex = read_ex()

    sea_cc = pd.merge(
        left=sea_raw, right=cc, how="left", left_on=["country"], right_on=["alpha-3"]
    )

    keep_columns = [
        "countrycode",
        "variable",
        "description",
        "code",
        *range(2000, 2015),
    ]

    sea = sea_cc[keep_columns]

    id_vars = [
        "countrycode",
        "variable",
        "description",
        "code",
    ]
    value_vars = list(range(2000, 2015))

    # Original table has one column per year.
    # We melt it, so we get 'year' column.
    sea_melted = pd.melt(
        sea, id_vars=id_vars, value_vars=value_vars, var_name="year", value_name="value"
    )

    sea_melted.dropna(inplace=True)
    sea_melted.drop_duplicates(inplace=True)

    # Original table has column variable with four possible values.
    # We change it into four columns.

    # So we have one row per country, counterpart, year observation.
    sea_grouped = (
        sea_melted.groupby(["countrycode", "description", "code", "year", "variable"])[
            "value"
        ]
        .aggregate("mean")
        .unstack()
    )

    sea_grouped.reset_index(inplace=True)

    int_cols = ["countrycode", "year"]
    real_cols = [
        "GO_PI",
        "GO_QI",
        "II_PI",
        "II_QI",
        "VA_PI",
        "VA_QI",
        "EMP",
        "EMPE",
        "CAP",
        "LAB",
        "COMP",
        "GO",
        "II",
        "VA",
        "H_EMPE",
        "K",
    ]

    for col in int_cols:
        sea_grouped[col] = asint(sea_grouped[col])

    for col in real_cols:
        sea_grouped[col] = asreal(sea_grouped[col])

    factorThousand = ["EMP", "EMPE"]
    factorMillion = [
        "CAP",
        "LAB",
        "COMP",
        "GO",
        "II",
        "VA",
        "H_EMPE",
        "K",
    ]

    for col in factorThousand:
        sea_grouped[col] = sea_grouped[col] * 1_000

    for col in factorMillion:
        sea_grouped[col] = sea_grouped[col] * 1_000_000

    sea_grouped.index.name = None
    sea_grouped = sea_grouped.rename_axis(None, axis=1)

    sea_ex = pd.merge(
        left=sea_grouped,
        right=ex,
        how="left",
        left_on=["countrycode", "year"],
        right_on=["countrycode", "year"],
    ).dropna(subset=["ex"])

    convert_cols = [
        "GO",
        "II",
        "VA",
        "CAP",
        "LAB",
        "COMP",
        "K",
    ]

    # Some columns are in local currency.
    for col in convert_cols:
        sea_ex[col] = sea_ex[col] / sea_ex["ex"]

    sea_ex["sectorcode"] = sea_ex["code"]
    sea_ex["sectorname"] = sea_ex["description"]

    out_cols = [
        "countrycode",
        "sectorcode",
        "sectorname",
        "year",
        "GO_PI",
        "GO_QI",
        "II_PI",
        "II_QI",
        "VA_PI",
        "VA_QI",
        "EMP",
        "EMPE",
        "CAP",
        "LAB",
        "COMP",
        "GO",
        "II",
        "VA",
        "H_EMPE",
        "K",
    ]

    sea_ex[out_cols].to_parquet(out_path, compression="gzip")


if __name__ == "__main__":
    typer.run(main)
