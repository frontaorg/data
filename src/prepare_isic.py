from pathlib import Path

import pandas as pd
import typer

isic3_path = Path("data/raw/isic/ISIC_Rev_3_english_structure.tsv")
isic31_path = Path("data/raw/isic/ISIC_Rev_3_1_english_structure.csv")
isic4_path = Path("data/raw/isic/ISIC_Rev_4_english_structure.csv")
conv3_31_path = Path("data/raw/isic/ISIC_Rev_3-ISIC_Rev_3_1_correspondence.csv")
conv31_4_path = Path("data/raw/isic/ISIC31_ISIC4.csv")

isic3_out_path = Path("data/out/isic/isic3.parquet.gzip")
isic31_out_path = Path("data/out/isic/isic3_1.parquet.gzip")
isic4_out_path = Path("data/out/isic/isic4.parquet.gzip")
conv3_31_out_path = Path("data/out/isic/isic3_isic3_1.parquet.gzip")
conv31_4_out_path = Path("data/out/isic/isic3_1_isic4.parquet.gzip")
conv3_4_out_path = Path("data/out/isic/isic3_isic4.parquet.gzip")


def create_classification(df: pd.DataFrame, which: str) -> pd.DataFrame:
    rows = []

    curr_category_code = None
    curr_category_description = None

    for _, series in df.iterrows():
        code: str = series["Code"]
        description: str = series["Description"]

        if code.isnumeric():
            rows.append(
                {
                    "categorycode": curr_category_code,
                    "categoryname": curr_category_description,
                    f"{which}code": code,
                    f"{which}name": description,
                }
            )
        else:
            curr_category_code = code
            curr_category_description = description

    result = pd.json_normalize(rows)

    return result


def main():
    isic3 = create_classification(
        pd.read_csv(isic3_path, sep="\t", dtype=str), which="isic3"
    )
    isic31 = create_classification(pd.read_csv(isic31_path, dtype=str), which="isic3.1")
    isic4 = create_classification(pd.read_csv(isic4_path, dtype=str), which="isic4")

    isic3.to_parquet(isic3_out_path, compression="gzip")
    isic31.to_parquet(isic31_out_path, compression="gzip")
    isic4.to_parquet(isic4_out_path, compression="gzip")

    conv3_31_raw = pd.read_csv(conv3_31_path, dtype=str)
    conv3_31_raw["isic3code"] = conv3_31_raw["Rev3"]
    conv3_31_raw["isic3.1code"] = conv3_31_raw["Rev31"]
    conv3_31_raw["is_partial"] = (conv3_31_raw["partial3"] == "1") | (
        conv3_31_raw["partial31"] == "1"
    )

    conv3_31 = (
        conv3_31_raw[["isic3code", "isic3.1code", "is_partial"]]
        .dropna()
        .drop_duplicates()
        .reset_index()
    )

    conv3_31[["isic3code", "isic3.1code", "is_partial"]].to_parquet(
        conv3_31_out_path, compression="gzip"
    )

    conv31_4_raw = pd.read_csv(conv31_4_path, dtype=str)
    conv31_4_raw["isic3.1code"] = conv31_4_raw["ISIC31code"]
    conv31_4_raw["isic4code"] = conv31_4_raw["ISIC4code"]
    conv31_4_raw["is_partial"] = (conv31_4_raw["partialISIC31"] == "1") | (
        conv31_4_raw["partialISIC4"] == "1"
    )

    conv31_4 = (
        conv31_4_raw[["isic3.1code", "isic4code", "is_partial"]]
        .dropna()
        .drop_duplicates()
        .reset_index()
    )

    conv31_4[["isic3.1code", "isic4code", "is_partial"]].to_parquet(
        conv31_4_out_path, compression="gzip"
    )

    conv3_4_raw = pd.merge(
        left=conv3_31,
        right=conv31_4,
        how="outer",
        left_on=["isic3.1code"],
        right_on=["isic3.1code"],
        suffixes=("_to3.1", "_from3.1"),
    )

    conv3_4_raw["is_partial"] = (
        conv3_4_raw["is_partial_to3.1"] | conv3_4_raw["is_partial_from3.1"]
    )

    conv3_4 = (
        conv3_4_raw[["isic3code", "isic4code", "is_partial"]]
        .dropna()
        .drop_duplicates()
        .reset_index()
    )

    conv3_4[["isic3code", "isic4code", "is_partial"]].to_parquet(
        conv3_4_out_path, compression="gzip"
    )


if __name__ == "__main__":
    typer.run(main)
